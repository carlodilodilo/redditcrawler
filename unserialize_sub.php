<?php 

/**-------------------------------------------------------------------------**/
/* CONFIGURE YOUR LISITNGS BELOW */	

//SET TIMEZONE
$settings['timezone']  = "UTC";

//GENERAL CONFIGS
$settings['subreddit'] = !empty($_GET['sub']) ? $_GET['sub'] : "NaturePics";
$settings['limit']     = !empty($_GET['lim']) ? $_GET['lim'] : 10;
$settings['override']  = !empty($_GET['override']) ? $_GET['override'] : null;

$subreddits = array(
    'todayilearned'
    // , 'LifeProTips'
    // , 'lifehacks'
    // , 'YouShouldKnow'
  );

foreach ( $subreddits as $subreddit ) {
  $cache = unserialize( file_get_contents( "cache/" . $subreddit ) );
  $cache_id_exist = file_get_contents( "cache/id_exist/" . $subreddit . '_exist' );
  $cache_id_exist = json_decode( $cache_id_exist, true );

  $meta_time = date( "Y-m-d H:i:s", $cache['meta']['time'] );
  $meta_subreddit = $cache['meta']['subreddit'];

  $id_exist = $cache_id_exist;

  $i = 0;
  foreach ( $cache as $key => $val ) {
    $val['preview'] = json_encode($val['preview']);
    $val['date'] = date( "Y-m-d H:i:s", $val['date'] );

    if ( $val['ups'] >= 100 ) {
      $id_exist[$val['id']] = $val['id'];

      print "<pre>";
      print_r( $val );
      print "</pre>";
      print "<br />";
      print "<br />";

      $i++;
    }

    file_put_contents( "cache/id_exist/" . $subreddit . '_exist', json_encode($id_exist) );
  }

  echo $i;
}
exit;

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/**-------------------------------------------------------------------------**/
/** INITIAL CHECKS, DON'T ALTER THE CODE UNLESS YOU KNOW WHAT YOU'RE DOING! **/
/**-------------------------------------------------------------------------**/

//CHECK WHETHER THE CORE FILES EXISTS
if(!file_exists('includes/reddit.php')) {
	die("Please check that all the files are in the correct root directory!");
}

//CHECK WHETER THE CACHE FOLDER EXISTS, IF NOT CREATE IT
if (!file_exists("cache")&&!is_dir("cache")) {
	mkdir("cache");
	header("Location: //".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
}

//LOAD FUNCTIONS FILE
require_once('includes/reddit.php');

//SET SETTINGS
date_default_timezone_set($settings['timezone']);

//DEFINE NEW VARIABLE
$reddit = new reddit;

//SUBREDDIT TO DISPLAY
$posts = $reddit->get_posts($settings['subreddit'],$settings['limit'],$settings['override']);
$cacheAge = $reddit->getCacheAge($settings['subreddit']);
