<?php 

/**-------------------------------------------------------------------------**/
/* CONFIGURE YOUR LISITNGS BELOW */	

//SET TIMEZONE
$settings['timezone']  = "UTC";

//GENERAL CONFIGS
$settings['subreddit'] = !empty($_GET['sub']) ? $_GET['sub'] : "NaturePics";
$settings['limit']     = !empty($_GET['lim']) ? $_GET['lim'] : 200;
$settings['override']  = !empty($_GET['override']) ? $_GET['override'] : null;

$subreddits = array(
    'todayilearned'
    // , 'LifeProTips'
    // , 'lifehacks'
    // , 'YouShouldKnow'
  );

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/**-------------------------------------------------------------------------**/
/** INITIAL CHECKS, DON'T ALTER THE CODE UNLESS YOU KNOW WHAT YOU'RE DOING! **/
/**-------------------------------------------------------------------------**/

//CHECK WHETHER THE CORE FILES EXISTS
if(!file_exists('includes/reddit.php')) {
	die("Please check that all the files are in the correct root directory!");
}

//CHECK WHETER THE CACHE FOLDER EXISTS, IF NOT CREATE IT
if (!file_exists("cache")&&!is_dir("cache")) {
	mkdir("cache");
	header("Location: //".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
}

//LOAD FUNCTIONS FILE
require_once('includes/reddit.php');

//SET SETTINGS
date_default_timezone_set($settings['timezone']);

//DEFINE NEW VARIABLE
$reddit = new reddit;

foreach ( $subreddits as $subreddit ) {
  //SUBREDDIT TO DISPLAY
  $posts = $reddit->get_posts( $subreddit, $settings['limit'], $settings['override'] );
  $cacheAge = $reddit->getCacheAge( $subreddit );
}